## Membre du projet 
GNANGNON Prince Brummell SIL3
GARBA Mouhamed SIL3
DEDO Serge SIL3
BIKOUKOU Espoire RIT3
AGBETE Gloria SIL3
# Uvendler
![](https://img.shields.io/badge/pattern-MVC-blue)
![](https://img.shields.io/badge/PHP-8.2.12-darkblue)
![](https://img.shields.io/badge/Symfony-vesion%205.8.2-darkgreen)
![](https://img.shields.io/badge/Symfony%20version-stable-blue)
![](https://img.shields.io/badge/contact-(229)%2097%20985%20109%2F54%20116%20302-blueviolet)

Une plateforme web robuste développée pour la gestion complète des
événements universitaires, favorisant l'interaction et la participation de la communauté
étudiante.

<br/>


## Objectifs

- Création d'un calendrier interactif permettant la visualisation des événements à venir.
- Mise en place d'un système de création d'événements avec des pages détaillées.
- Facilitation de l'inscription en ligne pour les étudiants intéressés.
- Intégration d'un système de notation et de commentaires pour chaque événement.
- Développement d'un tableau de bord personnalisé pour les organisateurs d'événements.
- Mise en place de notifications pour informer les utilisateurs des mises à jour et des nouveaux
événements.


## Échéancier

- **Phase 1 :** Développement du calendrier interactif et du système de création d'événements.
- **Phase 2 :** Intégration du système d'inscription en ligne et de la promotion.
- **Phase 3 :** Ajout du système de notation, de commentaires et du tableau de bord.
- **Phase 4 :** Implémentation des notifications et finalisation du projet.


## Technologies Utilisées

- Symfony pour le développement back-end.
- Base de données MySQL pour le stockage des informations.
- HTML, CSS, et JavaScript pour le développement front-end.
- Intégration d'APIs pour les fonctionnalités de partage sur les réseaux sociaux.

# Documentation

## Environnement de développement
Pour démarrer le serveur de développement, il faut ce placer dans le dossier racine du projet,
ensuite ouvrir un terminal dans ce dossier, et enfin exécuter la commande suivante dans le
terminal.

```sh
symfony server:start
```

Par défaut, l'application sera accessible sur ce lien :
[http://127.0.0.1:8000](http://127.0.0.1:8000)
ou ce lien [http://localhost:8000](http://localhost:8000).
